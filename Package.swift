// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "DeviceCache",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "DeviceCache",
            targets: ["DeviceCache"]
        ),
        .library(
            name: "ImageCache",
            targets: ["ImageCache"]
        )
    ],
    targets: [
        .target(
            name: "DeviceCache"
        ),
        .target(
            name: "ImageCache",
            dependencies: [
                "DeviceCache"
            ]
        )
    ]
)
