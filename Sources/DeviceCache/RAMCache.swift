//
//  Created on 27.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

/// Кеш в оперативной памяти (`RAM` - Random Access Memory).
/// Запись и чтение осуществляется с помощью `NSCache`.
public actor RAMCache<Value>: Cache {

    // MARK: - subtypes

    public typealias Container = CacheContainer<Value>

    // MARK: - variables

    public let maxExpiry: CacheExpiry

    private let cache = NSCache<NSString, Container>()

    // MARK: - init

    /// Инициализация RAM кеша.
    /// - Parameters:
    ///   - maxCount: Максимальное количество объектов, которые должен содержать кэш.
    ///   - maxCapacity: Максимальная общая стоимость, которую кэш может хранить до того, как он начнет высеивать объекты.
    ///   - maxExpiry: Максимальный срок жизни кешированных данных.
    public init(
        maxCount: Int,
        maxCapacity: Int,
        maxExpiry: CacheExpiry
    ) {
        self.maxExpiry = maxExpiry
        self.cache.countLimit = maxCount
        self.cache.totalCostLimit = maxCapacity
    }

    // MARK: - accessors

    public func getContainer(forKey key: String) async throws -> Container? {
        let key = key as NSString

        guard let container = self.cache.object(forKey: key) else {
            return nil
        }

        if container.isExpired {
            self.cache.removeObject(forKey: key)
            return nil
        } else {
            return container
        }
    }

    public func setContainer(_ container: Container?, forKey key: String) async throws {
        let key = key as NSString

        if let container {
            self.cache.setObject(container, forKey: key)
        } else {
            self.cache.removeObject(forKey: key)
        }
    }

    public func clear() async throws {
        self.cache.removeAllObjects()
    }
}
