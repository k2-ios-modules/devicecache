//
//  Created on 27.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol ValueCache: Cache {
    associatedtype RAMValue
    associatedtype FMValue: Codable

    var ramCache: RAMCache<RAMValue>? { get }
    var fmCache: FMCache<FMValue>? { get }
}

public extension ValueCache {
    func clear() async throws {
        try await self.ramCache?.clear()
        try await self.fmCache?.clear()
    }
}
