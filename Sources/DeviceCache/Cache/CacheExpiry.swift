//
//  Created on 27.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum CacheExpiry {
    public static var distantFuture: Self { .custom(.distantFuture) }

    case seconds(Int)
    case minutes(Int)
    case hours(Int)
    case days(Int)
    case custom(Date)

    public var date: Date {
        switch self {
        case .seconds(let seconds):
            return Date().append(.second, value: seconds)
        case .minutes(let seconds):
            return Date().append(.minute, value: seconds)
        case .hours(let seconds):
            return Date().append(.hour, value: seconds)
        case .days(let days):
            return Date().append(.day, value: days)
        case .custom(let date):
            return date
        }
    }
}

// MARK: - Comparable

extension CacheExpiry: Comparable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.date == rhs.date
    }

    public static func < (lhs: Self, rhs: Self) -> Bool {
        lhs.date < rhs.date
    }
}

// MARK: - Date

fileprivate extension Date {
    func append(_ component: Calendar.Component, value: Int) -> Date {
        guard let date = Calendar.current.date(byAdding: component, value: value, to: self) else {
            fatalError("Failed adding `\(component)` \(value) to date \(self)")
        }
        return date
    }
}
