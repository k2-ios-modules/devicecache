//
//  Created on 27.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

/// Контейнер кешируемого значения.
/// Кешируется не само значение, а сам контейнер со значением.
public final class CacheContainer<Value> {

    /// Дата "протухания" (скор жизни) кешируемого значения.
    public let expired: Date

    /// Кешируемое значение.
    public let value: Value
    
    /// Признак протухания.
    public var isExpired: Bool {
        self.expired < Date()
    }

    public init(
        _ value: Value,
        expiry: CacheExpiry = .distantFuture
    ) {
        self.value = value
        self.expired = expiry.date
    }
}

extension CacheContainer: Codable where Value: Codable { }
