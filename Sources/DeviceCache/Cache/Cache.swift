//
//  Created on 27.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

/// Описание сущности кеша.
public protocol Cache: Actor {
    /// Значение кеша.
    associatedtype Value

    /// Кешируемый контейнер.
    typealias Container = CacheContainer<Value>

    /// Максимальный срок годности (жизни) кеша.
    var maxExpiry: CacheExpiry { get }

    /// Получить кешируемый контейнер значения.
    /// - Parameter key: Ключ кешируемого значения.
    /// - Returns: Контейнер из кеша.
    func getContainer(forKey key: String) async throws -> Container?

    /// Записать контейнер в кеш.
    /// - Parameters:
    ///   - container: Кешируемый контейнер.
    ///   - key: Ключ кешируемого значения.
    func setContainer(_ container: Container?, forKey key: String) async throws

    /// Очистить кеш.
    func clear() async throws
}

public extension Cache {

    var maxExpiry: CacheExpiry { .distantFuture }

    /// Получить значение из кеша.
    /// - Parameter key: Ключ кешируемого значения.
    /// - Returns: Значение из кеша.
    func getValue(forKey key: String) async throws -> Value? {
        try await getContainer(forKey: key)?.value
    }
    
    /// Записать значение в кеш.
    /// - Parameters:
    ///   - value: Кешируемое значение.
    ///   - key: Ключ кешируемого значения.
    func setValue(_ value: Value?, forKey key: String) async throws {
        try await setValue(value, forKey: key, expiry: self.maxExpiry)
    }

    /// Записать значение в кеш.
    /// - Parameters:
    ///   - value: Кешируемое значение.
    ///   - key: Ключ кешируемого значения.
    ///   - expiry: Скор годности (жизни) значения.
    func setValue(_ value: Value?, forKey key: String, expiry: CacheExpiry) async throws {
        guard let value else {
            try await setContainer(nil, forKey: key)
            return
        }

        let expiry = min(expiry, maxExpiry)
        let container = Container(value, expiry: expiry)

        try await setContainer(container, forKey: key)
    }
}
