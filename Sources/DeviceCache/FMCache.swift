//
//  Created on 27.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import CryptoKit
import Foundation

/// Кеш в постоянной памяти, на диске (`FM` - Flash Memory).
/// Запись и чтение осуществляется с помощью `FileManager`.
public actor FMCache<Value: Codable>: Cache {

    // MARK: - subtypes

    public typealias Container = CacheContainer<Value>

    // MARK: - variables

    private let fileManager: FileManager

    private let directoryUrl: URL

    private let encoder: JSONEncoder

    private let decoder: JSONDecoder

    private let maxCount: Int

    private let maxCapacity: Int

    public let maxExpiry: CacheExpiry

    // MARK: - init

    /// Инициализация FM кеша.
    /// - Parameters:
    ///   - fileManager: Менеджер взаимодействия с файловом системой.
    ///   - makeDirectoryUrl: Формирование дирректории хранения данных.
    ///   - encoder: Кодировщик данных.
    ///   - decoder: Декодировщик данных.
    ///   - maxCount: Максимальное количество файлов, которые должен содержать кэш.
    ///   - maxCapacity: Максимальная объем данных (в байтах), которую кэш может хранить до того, как он начнет высеивать объекты.
    ///   - maxExpiry: Максимальный срок жизни кешированных данных.
    public init(
        fileManager: FileManager = .default,
        makeDirectoryUrl: (FileManager) -> URL,
        encoder: JSONEncoder,
        decoder: JSONDecoder,
        maxCount: Int,
        maxCapacity: Int,
        maxExpiry: CacheExpiry = .distantFuture
    ) throws {
        self.fileManager = fileManager
        let directoryUrl = makeDirectoryUrl(fileManager)
        self.directoryUrl = directoryUrl
        self.encoder = encoder
        self.decoder = decoder
        self.maxCount = maxCount
        self.maxCapacity = maxCapacity
        self.maxExpiry = maxExpiry

        try fileManager.createDirectory(at: directoryUrl, withIntermediateDirectories: true)
    }

    // MARK: - accessors

    public func getContainer(forKey key: String) async throws -> Container? {
        let fileUrl = self.directoryUrl.appendingPathComponent(key.md5)

        guard self.fileManager.fileExists(atPath: fileUrl.path) else {
            return nil
        }

        let data = try Data(contentsOf: fileUrl)
        let container = try self.decoder.decode(Container.self, from: data)

        if container.isExpired {
            try self.removeFile(at: fileUrl)
            return nil
        } else {
            try self.updateAccessDate(for: fileUrl)
            try self.cleanupFiles()

            return container
        }
    }

    public func setContainer(_ container: Container?, forKey key: String) async throws {
        let fileUrl = self.directoryUrl.appendingPathComponent(key.md5)

        guard let container else {
            try self.removeFile(at: fileUrl)
            return
        }

        let data = try self.encoder.encode(container)
        try data.write(to: fileUrl, options: .atomic)

        try self.updateAccessDate(for: fileUrl)
        try self.cleanupFiles()
    }

    public func clear() async throws {
        try self.fileManager.contentsOfDirectory(
            at: self.directoryUrl,
            includingPropertiesForKeys: nil
        ).forEach { fileUrl in
            try self.fileManager.removeItem(at: fileUrl)
        }
    }

    // MARK: - management

    private func removeFile(at fileUrl: URL) throws {
        guard self.fileManager.fileExists(atPath: fileUrl.path) else { return }
        try self.fileManager.removeItem(at: fileUrl)
    }

    private func updateAccessDate(for fileUrl: URL) throws {
        var fileUrl = fileUrl
        var resourceValues = URLResourceValues()
        resourceValues.contentAccessDate = Date()
        try fileUrl.setResourceValues(resourceValues)
    }

    private func cleanupFiles() throws {
        let fileUrls = try self.fileManager.contentsOfDirectory(
            at: self.directoryUrl,
            includingPropertiesForKeys: [.fileSizeKey, .contentAccessDateKey]
        ).sorted { lhs, rhs in
            guard let lhs = try lhs.resourceValues(forKeys: [.contentAccessDateKey]).contentAccessDate
            else { return false }

            guard let rhs = try rhs.resourceValues(forKeys: [.contentAccessDateKey]).contentAccessDate
            else { return true }

            return lhs.compare(rhs) == .orderedDescending
        }

        let fileSizes = try fileUrls.scan(0) { scanSize, fileUrl in
            let fileSize: Int = try fileUrl.resourceValues(forKeys: [.fileSizeKey]).fileSize ?? 0
            return scanSize + fileSize
        }
        .dropFirst()

        for (fileUrl, fileSize) in zip(fileUrls, fileSizes)
        where fileSize > self.maxCapacity {
            try self.removeFile(at: fileUrl)
        }

        for (fileIndex, fileUrl) in fileUrls.enumerated()
        where fileIndex + 1 > self.maxCount {
            try self.removeFile(at: fileUrl)
        }
    }

}

// MARK: - Sequence

fileprivate extension Sequence {
    func scan<Result>(_ first: Result, _ next: (Result, Element) throws -> Result) rethrows -> [Result] {
        var result = [first]
        for value in self {
            result.append(try next(result.last!, value))
        }
        return result
    }
}

// MARK: - MD5

fileprivate extension String {
    var md5: String {
        Insecure.MD5
            .hash(data: Data(self.utf8))
            .map({ String(format: "%02hhx", $0) })
            .joined()
    }
}
