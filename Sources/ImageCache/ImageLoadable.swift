//
//  Created on 12.06.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

/// Протокол загрузки изображения.
public protocol ImageLoadable: AnyObject {

    /// Задача загрузки картинки из сети.
    var imageLoadingTask: Task<Void, Never>? { get set }

    /// Установить загруженную картинку из сети или кеша.
    /// - Parameter image: Целевая картинка.
    func setLoadedImage(_ image: UIImage?)
}

public extension ImageLoadable {
    /// Отменить загрузку и установить картинку по-умолчанию.
    /// - Parameter placeholder: Картинка по-умолчанию.
    func cancelLoadingImage(placeholder: UIImage? = nil) {
        self.imageLoadingTask?.cancel()
        self.imageLoadingTask = nil
        self.setLoadedImage(placeholder)
    }

    /// Загрузить картирку из сети или кеша.
    /// - Parameters:
    ///   - request: Запрос для загрузки из сети.
    ///   - imageDownloader: Загрузчик картинки.
    ///   - deviceCache: Кеш картинок.
    ///   - placeholder: Картинка по-умолчанию.
    ///   - adjustmentImage: Корректировка загружаемой картинки.
    func loadImage(
        request: URLRequest?,
        imageDownloader: ImageDownloader,
        deviceCache: ImageCache?,
        placeholder: UIImage?,
        adjustmentImage: ((UIImage?) -> UIImage?)? = nil
    ) {
        guard let request, let key = request.url?.absoluteString else {
            self.cancelLoadingImage(placeholder: placeholder)
            return
        }
        self.imageLoadingTask = Task { @MainActor in
            do {
                let image: UIImage?

                if let deviceCache, let cached = try await deviceCache.getValue(forKey: key) {
                    image = cached
                } else {
                    let task = imageDownloader.imageDownload(request: request)
                    try Task.checkCancellation()
                    let data = try await task.value
                    image = UIImage(data: data)

                    if let deviceCache, let image {
                        try await deviceCache.setValue(image, forKey: key)
                    }
                }

                self.setLoadedImage(adjustmentImage?(image) ?? image ?? placeholder)
            } catch {
                self.setLoadedImage(placeholder)
            }
        }
    }
}
