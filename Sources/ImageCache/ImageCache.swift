//
//  Created on 12.06.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import DeviceCache
import Foundation
import UIKit

public actor ImageCache: ValueCache {

    // MARK: - subtypes

    public typealias Container = CacheContainer<UIImage>

    // MARK: - variables

    public let ramCache: RAMCache<UIImage>?

    public let fmCache: FMCache<Data>?

    // MARK: - init

    public init(
        ramCache: RAMCache<UIImage>?,
        fmCache: FMCache<Data>?
    ) {
        self.ramCache = ramCache
        self.fmCache = fmCache
    }

    // MARK: - accessors

    public func getContainer(forKey key: String) async throws -> Container? {
        if let container = try await self.ramCache?.getContainer(forKey: key) {
            return container
        }

        if let dataContainer = try await self.fmCache?.getContainer(forKey: key),
           let value = UIImage(data: dataContainer.value) {
            let container = Container(value, expiry: .custom(dataContainer.expired))
            try await self.ramCache?.setContainer(container, forKey: key)
            return container
        }

        return nil
    }

    public func setContainer(_ container: Container?, forKey key: String) async throws {
        var dataContainer: CacheContainer<Data>?

        if let container, let data = container.value.pngData() {
            dataContainer = CacheContainer(data, expiry: .custom(container.expired))
        }

        try await self.ramCache?.setContainer(container, forKey: key)
        try await self.fmCache?.setContainer(dataContainer, forKey: key)
    }
}

extension FileManager: @unchecked Sendable { }
