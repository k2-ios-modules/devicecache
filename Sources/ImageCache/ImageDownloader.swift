//
//  Created on 12.06.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

/// Протокол загрузчика изображений.
public protocol ImageDownloader {
    /// Загрузить изображение по запросу.
    /// - Parameter request: Запрос для загрузки из сети.
    /// - Returns: Задача загрузки бинарных данных изображения.
    func imageDownload(request: URLRequest?) -> Task<Data, Error>
}
